virtualenv ../env_wordlist
source ../env_wordlist/bin/activate
pip install -r requirements.txt
cd wordlist
chmod +x manage.py
./manage.py migrate
./manage.py loaddata initial_data.json
./manage.py createsuperuser
./manage.py runserver