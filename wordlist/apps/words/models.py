from __future__ import unicode_literals

from django.db import models


class Word(models.Model):

    title = models.CharField(max_length=100)
    defination = models.CharField(max_length=2000)

    class Meta:
        verbose_name = "Word"
        verbose_name_plural = "Words"

    def __str__(self):
        return self.title

    def __unicode__(self):
        return unicode(self.title) or u''
