from django.contrib import admin
from .models import Word


class WordAdmin(admin.ModelAdmin):
    list_display = ['title', 'defination']
    search_fields = ['title', 'defination']

admin.site.register(Word, WordAdmin)