from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

import csv
import os

from wordlist.apps.words.models import Word


class Command(BaseCommand):
    help = 'Utility command ./manage.py load_words'

    def handle(self, *args, **options):
        DATA_DIR = os.path.join(getattr(settings, 'BASE_DIR'), 'wordlist.csv')
        f = open(DATA_DIR, 'rb')
        reader = csv.reader(f)
        next(reader, None)
        for row in reader:
            k = {}
            k['title'] = row[0]
            k['defination'] = row[1]
            print k['title']
            bb, created = Word.objects.get_or_create(**k)
