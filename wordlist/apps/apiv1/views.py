from django.shortcuts import render, get_object_or_404

from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from .serializers import WordSerializer
from wordlist.apps.words.models import Word


# not being used
class WordList(generics.ListAPIView):
    serializer_class = WordSerializer
    queryset = Word.objects.all()
    paginate_by = 10
    max_paginate_by = 100

    def get(self, request, *args, **kwargs):
        words = Word.objects.all()
        serializer = self.serializer_class(words, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class WordListByLetter(generics.ListAPIView):
    serializer_class = WordSerializer
    queryset = Word.objects.all()

    def get(self, request, *args, **kwargs):
        letter = self.kwargs.get('letter', 'a')
        words = Word.objects.filter(title__startswith=letter)
        serializer = self.serializer_class(words, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class WordDetail(generics.RetrieveAPIView):
    serializer_class = WordSerializer
    queryset = Word.objects.all()

    def get(self, request, *args, **kwargs):
        title = self.kwargs.get('word', None)
        words = Word.objects.filter(title=title)
        serializer = self.serializer_class(words, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
