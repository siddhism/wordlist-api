from django.conf.urls import url

from .views import WordList, WordListByLetter, WordDetail

app_name = 'apiv1'
urlpatterns = [
    # url('^words/$', WordList.as_view(), name='wordlist'),
    url('^words/(?P<letter>\w+)$', WordListByLetter.as_view(), name='wordlistletter'),
    url('^word/(?P<word>\w+)$', WordDetail.as_view(), name='worddetail'),
]
