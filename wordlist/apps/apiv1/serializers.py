from rest_framework import serializers

from wordlist.apps.words.models import Word


class WordSerializer(serializers.ModelSerializer):
    "Word serializer"

    class Meta:
        model = Word
        fields = ('title', 'defination')

